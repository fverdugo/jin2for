
function sum_i(a,b) result (c)
  implicit none
  integer, intent(in) :: a
  integer, intent(in) :: b
  integer :: c
  c = a + b
end function

function sum_r(a,b) result (c)
  implicit none
  real, intent(in) :: a
  real, intent(in) :: b
  real :: c
  c = a + b
end function


