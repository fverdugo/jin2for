function sum_i1(a,b) result (c)
  implicit none
  integer(1), intent(in) :: a
  integer(1), intent(in) :: b
  integer(1) :: c
  c = a + b
end function

function sum_i2(a,b) result (c)
  implicit none
  integer(2), intent(in) :: a
  integer(2), intent(in) :: b
  integer(2) :: c
  c = a + b
end function

function sum_i4(a,b) result (c)
  implicit none
  integer(4), intent(in) :: a
  integer(4), intent(in) :: b
  integer(4) :: c
  c = a + b
end function

function sum_i8(a,b) result (c)
  implicit none
  integer(8), intent(in) :: a
  integer(8), intent(in) :: b
  integer(8) :: c
  c = a + b
end function

function sum_i16(a,b) result (c)
  implicit none
  integer(16), intent(in) :: a
  integer(16), intent(in) :: b
  integer(16) :: c
  c = a + b
end function

function sum_r4(a,b) result (c)
  implicit none
  real(4), intent(in) :: a
  real(4), intent(in) :: b
  real(4) :: c
  c = a + b
end function

function sum_r8(a,b) result (c)
  implicit none
  real(8), intent(in) :: a
  real(8), intent(in) :: b
  real(8) :: c
  c = a + b
end function

function sum_r10(a,b) result (c)
  implicit none
  real(10), intent(in) :: a
  real(10), intent(in) :: b
  real(10) :: c
  c = a + b
end function

function sum_r16(a,b) result (c)
  implicit none
  real(16), intent(in) :: a
  real(16), intent(in) :: b
  real(16) :: c
  c = a + b
end function


