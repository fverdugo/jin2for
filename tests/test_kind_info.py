from jin2for.kind_info import KindInfo

from jin2for_fixtures import testdata

import os

def test_load(testdata):

    kind_info = KindInfo()

    kindsfile = os.path.join(testdata,'kinds_gfortran.json')

    kind_info.load(kindsfile)

def test_dump(testdata,tmp_path):

    kind_info = KindInfo()

    kindsfile = os.path.join(testdata,'kinds_gfortran.json')

    kindsfile_bis = str( tmp_path / 'kinds_gfortran-bis.json' )

    kind_info.load(kindsfile)

    kind_info.dump(kindsfile_bis)

    kind_info_bis = KindInfo()

    kind_info_bis.load(kindsfile_bis)

    assert kind_info.integer_kinds == kind_info_bis.integer_kinds
    assert kind_info.real_kinds == kind_info_bis.real_kinds
    assert kind_info.character_kinds == kind_info_bis.character_kinds
    assert kind_info.logical_kinds == kind_info_bis.logical_kinds

def test_generate():

    kind_info = KindInfo()

    kind_info.generate('gfortran')
