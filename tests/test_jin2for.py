# This file is part of the jin2for project
# (c) 2019 Francesc Verdugo <fverdugo@cimne.upc.edu>

import shlex
import filecmp
import jin2for
from pytest import fixture
import os

from jin2for_fixtures import testdata

@fixture(autouse=True)
def run_around_tests(tmp_path):
    cwd = os.getcwd()
    os.chdir(str(tmp_path))
    yield
    os.chdir(cwd)

def run(s):

    cml = shlex.split(s)
    app = jin2for.App(cml)
    app.run()

def test_1(testdata):

    i = "-I{0:} {0:}/{1:}".format(testdata,'test_1.t90')

    run(i)

    o1 = os.path.join(testdata,'test_1_result.f90')
    o2 = 'test_1.f90'
    assert filecmp.cmp(o1,o2)

def test_2(testdata):

    i = "-I{0:} --generate-for gfortran {0:}/{1:}".format(testdata,'test_2.t90')

    run(i)

    o1 = os.path.join(testdata,'test_2_result.f90')
    o2 = 'test_2.f90'
    assert filecmp.cmp(o1,o2)

def test_3(testdata):

    i = "-I{0:} {0:}/{1:}".format(testdata,'test_3.t90')

    run(i)

    o1 = os.path.join(testdata,'test_3_result.f90')
    o2 = 'test_3.f90'
    assert filecmp.cmp(o1,o2)

def test_4(testdata):

    i = "-I{0:} --generate-from {0:}/{2:} {0:}/{1:}".format(
        testdata,'test_2.t90','kinds_gfortran.json')

    run(i)

    o1 = os.path.join(testdata,'test_2_result.f90')
    o2 = 'test_2.f90'
    assert filecmp.cmp(o1,o2)
