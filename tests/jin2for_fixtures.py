# This file is part of the jin2for project
# (c) 2019 Francesc Verdugo <fverdugo@cimne.upc.edu>

from pytest import fixture
import os

@fixture
def testdata(request):
    filename = request.module.__file__
    d = os.path.dirname(filename)
    return os.path.join(d,'data')

